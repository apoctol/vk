﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Model;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        VkApi api = new VkApi();
        //string token;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie cookieReq = Request.Cookies["My cookie"];
            string path = null;
            if (cookieReq != null)
            {
                string login = cookieReq["login"];
                string pass = cookieReq["pass"];
                api.Authorize(new ApiAuthParams
                {
                    ApplicationId = 7095016,
                    Login = login,
                    Password = pass,
                    Settings = Settings.All

                });
                var u = api.Users.Get(new long[] { Convert.ToInt64(api.UserId) }).FirstOrDefault();
                path += "Username" + "<p></p>" + u.FirstName + "<p></p>" + "Friend's name" + "<p></p>";
                var friends = api.Friends.Get(new VkNet.Model.RequestParams.FriendsGetParams
                {
                    Count = 5,
                    Fields = ProfileFields.FirstName,
                });
                foreach (var item in friends)
                {
                    path += item.FirstName + "<p></p>";
                }
                text.Text = path;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string path = null;
            if (api.Token == null)
            {
                api.Authorize(new ApiAuthParams
                {
                    ApplicationId = 7095016,
                    Login = Login.Text,
                    Password = Pass.Text,
                    Settings = Settings.All

                });
                HttpCookie cookie = new HttpCookie("My cookie");
                cookie["login"] = Login.Text;
                cookie["pass"] = Pass.Text;
                Response.Cookies.Add(cookie);
            }
            else
            {
                
            }
           
            var u = api.Users.Get(new long[] { Convert.ToInt64(api.UserId) }).FirstOrDefault();
            path += "Username"+ "<p></p>" + u.FirstName + "<p></p>" +"Friend's name" + "<p></p>";
            var friends = api.Friends.Get(new VkNet.Model.RequestParams.FriendsGetParams
            {
                Count = 5,
                Fields = ProfileFields.FirstName,
            });
            foreach (var item in friends)
            {
                path += item.FirstName + "<p></p>";
            }
            text.Text = path;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

          
        }
    }
}